import 'package:flutter/material.dart';
import 'package:tesla_app/widgets/colores.dart';

class PresentationPage extends StatelessWidget {
  const PresentationPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colores.colorNegro,
      body: Center(
        child: Image.asset('assets/img/tesla.png'),
      ),
    );
  }
}
