import 'package:flutter/material.dart';
import 'package:tesla_app/widgets/colores.dart';

class MenuColores extends StatefulWidget {
  MenuColores({Key key}) : super(key: key);

  @override
  _MenuColoresState createState() => _MenuColoresState();
}

class _MenuColoresState extends State<MenuColores> {
  int selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 8),
      child: SizedBox(
        height: 100,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                setState(() {
                  selectedIndex = index;
                  /* Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Categorias(
                        tipoProducto: typeProduct,
                      ),
                    ),
                  );*/
                });
              },
              child: Container(
                margin: EdgeInsets.only(right: 5),
                width: 110,
                height: 40,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: selectedIndex == index
                      ? Colores.colorRojo
                      : Colores.colorNegro,
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
