import 'package:flutter/material.dart';

class Colores {
  static const Color colorPresentation = Color(0xFFffffff);
  static const Color colorPlomo = Color(0xfFcccaca);
  static const Color textosGris = Color(0xfF707070);
  static const Color colorRojo = Color(0xfFe82127);
  static const Color colorNegro = Color(0xFF0b0c0c);
}
